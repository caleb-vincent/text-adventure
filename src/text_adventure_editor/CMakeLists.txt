cmake_minimum_required(VERSION 3.12)

file(GLOB tae_SRC "*.?pp")


add_executable(ta_editor
   ${tae_SRC})

set_property(TARGET ta_editor PROPERTY CXX_STANDARD 20)

target_compile_options(ta_editor PRIVATE "-Wall")
target_compile_options(ta_editor PRIVATE "-Wpedantic")
target_compile_options(ta_editor PRIVATE "-Wextra")
target_compile_options(ta_editor PRIVATE "-Werror")


target_link_libraries(ta_editor
  PRIVATE text_adventure
  PRIVATE ftxui::screen
  PRIVATE ftxui::dom
  PRIVATE ftxui::component)

add_compile_definitions(TEXT_ADVENTURE_EDITOR)
