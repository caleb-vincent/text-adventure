//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <node.hpp>


#include <ftxui/dom/elements.hpp>
#include <ftxui/component/captured_mouse.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>
#include <ftxui/component/event.hpp>
#include <ftxui/component/screen_interactive.hpp>

#include <list>
#include <ranges>

int main(int, char**)
{
   using namespace ftxui;

   auto screen = ScreenInteractive::Fullscreen();


   int nodeSelected(0), lastNodeSelected(-1);
   std::string currentNodeId, currentNodeState;


   std::vector<text_adventure::Node> nodes
   {
      { "Test1" },
      { "Test2" },
      { "Test3" }
   };

   std::vector<std::string> nodeIds;
   std::ranges::for_each(nodes, [&nodeIds]
      (auto const& node)
      {
         nodeIds.emplace_back(node.id());
      });

   auto nodeMenu{ Menu(&nodeIds, &nodeSelected) };

   auto inputNodeId{ Input(&currentNodeId, "") };
   auto inputNodeState{ Input(&currentNodeState, "") };
   auto buttonSave{ Button("Save", [&]
                                    {
                                       nodeIds[nodeSelected] = currentNodeId;
                                       nodes[nodeSelected].m_id = currentNodeId;
                                       nodes[nodeSelected].m_state = currentNodeState;
                                    }) };

   auto buttonRevert{ Button("Revert", [&]
                                       {
                                          currentNodeId = nodes[nodeSelected].m_id;
                                          currentNodeState = nodes[nodeSelected].m_state;
                                       }) };

   auto layout = [&]
   {
      if(nodeSelected != lastNodeSelected)
      {
         currentNodeId = nodes[nodeSelected].m_id;
         currentNodeState = nodes[nodeSelected].m_state;
         lastNodeSelected = nodeSelected;
      }

      return hbox({
                  nodeMenu->Render(),
                  separator(),
                     vbox({
                        hbox({text("ID: ") | flex, inputNodeId->Render() | flex_grow}),
                        hbox({text("State: ") | flex, inputNodeState->Render() | flex_grow}),
                        separator(),
                        hbox({buttonSave->Render(), buttonRevert->Render()})
                        })
                  }) | border;
   };

   auto container = Container::Horizontal({nodeMenu,
                                                  Container::Vertical({Container::Horizontal({inputNodeId}),
                                                                       Container::Horizontal({inputNodeState}),
                                                                       Container::Horizontal({buttonSave, buttonRevert})
                                                                           })
                                                 });

   auto finalContainer = CatchEvent(container, [&](Event event)
                                                {
                                                   if (event == Event::Escape)
                                                   {
                                                      screen.ExitLoopClosure()();
                                                      return true;
                                                   }
                                                   return false;
                                                });


   screen.Loop(ftxui::Renderer(finalContainer, layout));
 }
