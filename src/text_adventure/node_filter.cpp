//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "node_filter.hpp"

#include <ctpg/ctpg.hpp>

using namespace text_adventure;


bool NodeFilter::matches(const Node& nodeToMatch, std::string_view const filterString)
{
      using Result_t = bool;

      static constexpr char wordPattern[] = "[a-zA-Z\\-]+";

      static constexpr ctpg::regex_term<wordPattern> id("id");

      static constexpr ctpg::nterm<Result_t> state("stateId");
      static constexpr ctpg::nterm<Result_t> node("nodeId");
      static constexpr ctpg::nterm<Result_t> propertyList("propertyList");
      static constexpr ctpg::nterm<Result_t> filterResult("filterResult");

      static constexpr ctpg::parser grammar(
          filterResult,
          terms(id, "*", ":", "{", "}", ",", "="),
          nterms(node, state,  filterResult, propertyList),
          rules(
            node("*")
               >= ctpg::ftors::val(true),
            node(id)
               >>= [](const Node &node, auto const& id_term){ return std::string_view(node.id()) == id_term; },

            state(":", "*")
               >= ctpg::ftors::val(true),
            state(":", id)
               >>= [](const Node &node, auto const&, auto const& id_term){ return std::string_view(node.state()) == id_term; },

            propertyList("{", id)
               >>= [](const Node & node, auto const&, auto const& id_term)
                  {
                     return node.property(std::string(id_term)) != "";
                  },
            propertyList(propertyList, ",", id)
               >>= [](const Node & node, auto const& prop, auto const&, auto const& id_term)
                  {
                     return prop && node.property(std::string(id_term)) != "";
                  },
            propertyList("{", id, "=", id)
               >>= [](const Node & node, auto const&, auto const& id_term, auto const&, auto const& value_term)
                  {
                     return node.property(std::string(id_term)) == std::string(value_term);
                  },
            propertyList(propertyList, ",", id, "=", id)
               >>= [](const Node & node, auto const& prop, auto const&, auto const& id_term, auto const&, auto const& value_term)
                  {
                     return prop && node.property(std::string(id_term)) == std::string(value_term);
                  },
            propertyList(propertyList, "}")
               >= [](auto const& prop, auto const&){ return prop; },

            filterResult(node, propertyList, state)
               >= [](auto const& node_term, auto const& prop_term, auto const& state_term)
                  {
                     return node_term && prop_term && state_term;
                  },
            filterResult(node, propertyList)
               >= [](auto const& node_term, auto const& prop_term)
                  {
                     return node_term && prop_term;
                  },
            filterResult(node, state)
               >= [](auto const& node_term, auto const& state_term)
                  {
                     return node_term && state_term;
                  },
            filterResult(node)
               >= [](auto const& node_term)
                  {
                     return node_term;
                  }
      )
    );

   (void)wordPattern;
   auto const result{ grammar.context_parse(nodeToMatch, ctpg::buffers::string_buffer(filterString.data())) };
   return result && *result;
}
