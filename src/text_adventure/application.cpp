//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "application.hpp"

#include "action.hpp"
#include "node.hpp"
#include "text_resources.hpp"

#include <functional>

using namespace text_adventure;
using namespace std::string_literals;


namespace
{

template<class... Ts> struct make_visitor : Ts... { using Ts::operator()...; };
template<class... Ts> make_visitor(Ts...) -> make_visitor<Ts...>;
}


Application::Application() :
   textResources{ new TextResources() },
   testNode{ new Node("test") },
   currentNode{ testNode.get() }
{}

Application::~Application()
{}

void Application::run()
{}

bool Application::input(const std::string& str)
{
   auto const res{ Action::Parse(str) };
   if(res.has_value())
   {

      std::visit(
         make_visitor
         {
            std::bind(&Application::onDrop, this, std::placeholders::_1),
            std::bind(&Application::onLook, this, std::placeholders::_1),
            std::bind(&Application::onMove, this, std::placeholders::_1),
            std::bind(&Application::onQuit, this, std::placeholders::_1),
            std::bind(&Application::onTake, this, std::placeholders::_1),
            std::bind(&Application::onUse, this, std::placeholders::_1)
         }, res.value());

   }
   return res.has_value();
}

std::string_view Application::location() const
{
   return currentNode->id();
}

std::string_view Application::description() const
{
   return currentNode->state();
}

bool Application::quitReq() const
{
   return quitRequested;
}

void Application::onDrop(Action::Drop const&)
{

}

void Application::onLook(Action::Look const& action)
{
   testText = action.target;
}

void Application::onMove(const Action::Move& move)
{
   (*currentNode)(move);
}

void Application::onQuit(Action::Quit const&)
{
   quitRequested = true;
}

void Application::onTake(Action::Take const&)
{

}

void Application::onUse(Action::Use const&)
{

}

