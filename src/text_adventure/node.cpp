//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "node.hpp"
#include "text_resources.hpp"

#include <cereal/archives/json.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

#include <algorithm>
#include <iterator>
#include <ranges>

using namespace text_adventure;

const std::string Node::ms_invalid{ "" };

namespace text_adventure
{
   template<class Archive>
   void serialize(Archive & archive, Node &node)
   {
      archive(node.m_id, node.m_state, CEREAL_NVP(node.m_neighbors));
   }
}

Node::Node(Id_t id) :
   m_id{ id },
   m_state{ "default" }
{}

Node::Node(Id_t id, std::istream& serialized) :
   m_id{ id },
   m_state{ "default" }
{
   cereal::JSONInputArchive archive(serialized);
   archive(*this);
   if(m_id != id)
   {
      // Exception? Warning message? Roll with it?
   }
}

Id_t const& Node::id() const
{
   return m_id;
}

State_t const& Node::state() const
{
   return m_state;
}

const Value_t& Node::property(const Property_t& prop) const
{
   return m_properties.contains(prop) ? m_properties.at(prop) : ms_invalid;
}

void Node::property(const Property_t& prop, Value_t value)
{
   if(m_properties.contains(prop))
   {
         m_properties.erase(prop);
   }
   m_properties.emplace(prop, value);
}

#include <iterator>
Result::Move Node::operator()(const Action::Move& action)
{
   if( std::ranges::find(m_neighbors, action.target ) != m_neighbors.end())
   {
      return { true, action.target };
   }

   return { false, "" };
}

std::string Node::save() const
{
   std::stringstream outputStream;
   // Cereal doesn't seem to finish serializing until the archive desctructs.
   {
      cereal::JSONOutputArchive archive(outputStream);
      archive(*this);
   }
   return outputStream.str();
}
