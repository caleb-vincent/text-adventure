#pragma once
//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "action.hpp"
#include "defs.hpp"
#include "result.hpp"

#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace text_adventure
{
   class Node
   {
   public:
      Node(Id_t id);
      Node(Id_t id, std::istream& serialized);

      Id_t const& id() const;

      State_t const& state() const;

      Value_t const& property(Property_t const& prop) const;

      void property(Property_t const& prop, Value_t value);

      Result::Move operator()(Action::Move const& action);

      std::string save() const;


   private:
      static const std::string ms_invalid;

      template<class Archive> friend void serialize(Archive &, Node &);

      // Don't really want to expose the ability to add/remove/check neighbors to everyone, but for now the tests need to
      friend class NodeEditor;


#ifdef TEXT_ADVENTURE_EDITOR
      // Pretty sure I don't like this either
     public:
#endif
      Id_t m_id;
      State_t m_state;

      std::vector<Id_t> m_neighbors;
      std::unordered_map<Property_t, Value_t> m_properties;
   };
}
