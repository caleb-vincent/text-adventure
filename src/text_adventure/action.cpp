//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "action.hpp"


#include <ctpg/ctpg.hpp>

#include <iostream>

using namespace text_adventure;

namespace
{
   constexpr char wordPattern[] = "[a-zA-Z\\-]+";
   constexpr ctpg::regex_term<wordPattern> word("word", 1);
   constexpr ctpg::nterm<Action::Drop> dropCommand("dropCommand");
   constexpr ctpg::nterm<Action::Look> lookCommand("lookCommand");
   constexpr ctpg::nterm<Action::Move> moveCommand("moveCommand");
   constexpr ctpg::nterm<Action::Take> takeCommand("takeCommand");
   constexpr ctpg::nterm<Action::Quit> quitCommand("quitCommand");
   constexpr ctpg::nterm<Action::Use>  useCommand("useCommand");
   constexpr ctpg::nterm<Action::Variant> generalCommand("generalCommand");

   constexpr ctpg::parser p(
       generalCommand,
       terms("drop", "look", "move", "go", "quit", "use", "take", "on", "with", word),
       nterms(generalCommand, dropCommand, lookCommand, moveCommand, takeCommand, quitCommand, useCommand),
       rules(
         dropCommand("drop", word)
            >=  [](const auto&, auto&& t) { return Action::Drop{ std::string(t) }; },
         lookCommand("look", word)
            >=  [](const auto&, auto&& t) { return Action::Look{ std::string(t) }; },
         moveCommand("move", word)
            >=  [](const auto&, auto&& t) { return Action::Move{ std::string(t) }; },
         moveCommand("go", word)
            >=  [](const auto&, auto&& t) { return Action::Move{ std::string(t) }; },
         takeCommand("take", word)
            >=  [](const auto&, auto&& t) { return Action::Take{ std::string(t) }; },
         quitCommand("quit")
            >=  ctpg::ftors::create<Action::Quit>{},
         useCommand("use", word, "on", word)
            >=  [](const auto&, auto&& tool, auto const&, auto&& target) { return Action::Use{ std::string(tool), std::string(target) }; },
         useCommand("use", word, "with", word)
            >=  [](const auto&, auto&& tool, auto const&, auto&& target) { return Action::Use{ std::string(tool), std::string(target) }; },
         generalCommand(dropCommand),
         generalCommand(lookCommand),
         generalCommand(moveCommand),
         generalCommand(takeCommand),
         generalCommand(quitCommand),
         generalCommand(useCommand)
     )
 );
}

std::optional<Action::Variant> Action::Parse(std::string const& str)
{
   return p.parse(ctpg::buffers::string_buffer(str.c_str()), std::cerr);
}
