//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "application.hpp"



#include <ftxui/dom/elements.hpp>
#include <ftxui/component/captured_mouse.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>
#include <ftxui/component/event.hpp>
#include <ftxui/component/screen_interactive.hpp>

#include <list>
#include <ranges>

int main(int, char**)
{
   using namespace ftxui;

   text_adventure::Application app;
   auto screen = ScreenInteractive::Fullscreen();


   std::string command;

   InputOption commandOption;
   commandOption.on_enter = [&]()
      {
         if(app.input(command))
         {
            command.clear();
         }
      };

   Component inputCommand = Input(&command, "", commandOption);

   auto layout = [&]
   {
      app.run();

      std::vector<Element> entries;

      constexpr std::string_view delim{"\n"};


      auto const description{ app.description() };

#if __GNUC__ > 11

      for (const auto para : std::views::split(app.description(), delim))
      {
         entries.push_back(paragraph(std::string(para.begin(), para.end())));
      }

      #pragma GCC warning "Compiler should now support spilitting std::string_view properly"

#else

      size_t startPos{ 0 }, endPos{ description.find_first_of(delim) };
      while(endPos != std::string::npos)
      {
         entries.push_back(paragraph(std::string(description.substr(startPos, endPos))));

         startPos = endPos + 1;
         if(startPos >= description.length())
         {
            break;
         }
         endPos = (description.find_first_of(delim, startPos));
      }
      if(startPos < description.length())
      {
         entries.push_back(paragraph(std::string(description.substr(startPos))));
      }


#endif


      if(app.quitReq())
      {
         screen.ExitLoopClosure()();
      }

      return vbox({
         window(text(std::string(app.location())), vbox(std::move(entries))),
         hbox({text("Command: "),
            inputCommand->Render()
         }),
      });
   };

   auto container = ftxui::Container::Vertical({inputCommand}) ;

   auto finalContainer = CatchEvent(container, [&](Event event)
                                                {
                                                   if (event == Event::Escape)
                                                   {
                                                      screen.ExitLoopClosure()();
                                                      return true;
                                                   }
                                                   return false;
                                                });


   screen.Loop(ftxui::Renderer(finalContainer, layout));
 }
