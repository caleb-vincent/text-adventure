//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <catch2/catch.hpp>

#include <action.hpp>

#include <array>

using namespace std::string_literals;

TEST_CASE( "quit command is interpreted", "[Action]")
{
   SECTION("just 'quit'")
   {
      auto res = text_adventure::Action::Parse("quit");
      REQUIRE(res.has_value());
      REQUIRE(std::holds_alternative<text_adventure::Action::Quit>(*res));
   }
   SECTION("whitespace before and after")
   {
      auto res = text_adventure::Action::Parse("\v\r\n\t  quit \t\v\r\n  ");
      REQUIRE(res.has_value());
      REQUIRE(std::holds_alternative<text_adventure::Action::Quit>(*res));
   }
}

TEST_CASE( "look command is interpreted", "[Action]")
{
   auto const look{ "look"s };
   std::vector<std::string> const targets {"up", "around", "left", "QWERRSdfgggrte", "a", "Z", "-", "a-Z", "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"};

   SECTION("just 'look', should not interpret")
   {
      auto res = text_adventure::Action::Parse(look);
      REQUIRE(!res.has_value());
   }
   SECTION("'look <target>'")
   {

      for( auto const&  target : targets)
      {
         auto res = text_adventure::Action::Parse(look + " " + target);
         REQUIRE(res.has_value());
         REQUIRE(std::holds_alternative<text_adventure::Action::Look>(*res));
         REQUIRE(std::get<text_adventure::Action::Look>(*res).target == target );
      }
   }

   SECTION("'look <target><whitespace> '")
   {

      for( auto const&  target : targets)
      {
         auto res = text_adventure::Action::Parse(look + " " + target + " \t\r\n\v");
         REQUIRE(res.has_value());
         REQUIRE(std::holds_alternative<text_adventure::Action::Look>(*res));
         REQUIRE(std::get<text_adventure::Action::Look>(*res).target == target );
      }
   }
}

TEST_CASE( "drop command is interpreted", "[Action]")
{
   auto const drop{ "drop"s };
   std::vector<std::string> const targets {"up", "around", "left", "QWERRSdfgggrte", "a", "Z", "-", "a-Z", "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"};

   SECTION("just 'drop', should not interpret")
   {
      auto res = text_adventure::Action::Parse(drop);
      REQUIRE_FALSE(res.has_value());
   }
   SECTION("'drop <target>'")
   {

      for( auto const&  target : targets)
      {
         auto res = text_adventure::Action::Parse(drop + " " + target);
         REQUIRE(res.has_value());
         REQUIRE(std::holds_alternative<text_adventure::Action::Drop>(*res));
         REQUIRE(std::get<text_adventure::Action::Drop>(*res).target == target );
      }
   }
}


TEST_CASE( "move command is interpreted", "[Action]")
{
   const auto moves {std::array{ "move"s, "go"s } };
   const auto targets {std::array{ "up", "over-there", "Left" } };

   SECTION("just 'move' or 'go', should not interpret")
   {
      for( auto const&  move : moves)
      {
         auto res = text_adventure::Action::Parse(move);
         REQUIRE(!res.has_value());
      }
   }

   SECTION("'move <target>'")
   {
      for( auto const&  move : moves)
      {
         for( auto const&  target : targets)
         {
            auto const str{ move + " " + target };
            INFO(str);
            auto res = text_adventure::Action::Parse(str);
            REQUIRE(res.has_value());
            REQUIRE(std::holds_alternative<text_adventure::Action::Move>(*res));
            REQUIRE(std::get<text_adventure::Action::Move>(*res).target == target );
         }
      }
   }
}


TEST_CASE( "take command is interpreted", "[Action]")
{
   auto const take{ "take"s };
   std::vector<std::string> const targets {"up", "around", "left", "QWERRSdfgggrte", "a", "Z", "-", "a-Z", "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"};

   SECTION("just 'take', should not interpret")
   {
      auto res = text_adventure::Action::Parse(take);
      REQUIRE_FALSE(res.has_value());
   }
   SECTION("'take <target>'")
   {

      for( auto const&  target : targets)
      {
         auto res = text_adventure::Action::Parse(take + " " + target);
         REQUIRE(res.has_value());
         REQUIRE(std::holds_alternative<text_adventure::Action::Take>(*res));
         REQUIRE(std::get<text_adventure::Action::Take>(*res).target == target );
      }
   }
}


TEST_CASE( "use command is interpreted", "[Action]")
{
   auto const use{ "use"s };
   std::vector<std::string> const words {"up", "around", "left", "QWERRSdfgggrte", "a", "Z", "-", "a-Z", "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"};
   std::vector<std::string> const prepositions { "on", "with" };


   SECTION("'use <tool> <prep> <target>'")
   {

      for( auto const&  tool : words)
      {
         for(auto const& prep : prepositions)
         {
            for(auto const& target : words)
            {
               auto const text{ use + " " + tool + " " + prep + " " + target };
               INFO(text);
               auto res = text_adventure::Action::Parse(text);
               REQUIRE(res.has_value());
               REQUIRE(std::holds_alternative<text_adventure::Action::Use>(*res));
               REQUIRE(std::get<text_adventure::Action::Use>(*res).tool == tool );
               REQUIRE(std::get<text_adventure::Action::Use>(*res).target == target );
            }
         }
      }
   }

}
