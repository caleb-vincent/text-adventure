//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <catch2/catch.hpp>

#include <action.hpp>
#include <node.hpp>
#include <result.hpp>

#include <cereal/types/vector.hpp>
#include <cereal/archives/json.hpp>

#include <algorithm>
#include <sstream>

namespace text_adventure
{
   struct NodeEditor
   {
      static void addNeighbor(Node& node, Id_t id)
      {
         node.m_neighbors.emplace_back(std::move(id));
      }

      static bool hasNeighbor(Node const& node, Id_t const& neighbor)
      {
         return std::ranges::find(node.m_neighbors, neighbor) != node.m_neighbors.end();
      }


      static void setState(Node& node, State_t state)
      {
         node.m_state = state;
      }
   };
}

TEST_CASE( "Serialization", "[Node]")
{
   constexpr auto ID{ "Test" };
   constexpr auto ID2{ "Test2" };
   constexpr auto STATE{ "Test-State" };
   text_adventure::Node nodeOne(ID);

   text_adventure::NodeEditor::setState(nodeOne, STATE);
   text_adventure::NodeEditor::addNeighbor(nodeOne, ID2);

   std::stringstream outStream(nodeOne.save());

   text_adventure::Node nodeTwo(ID, outStream);

   REQUIRE(nodeTwo.id() == ID);
   REQUIRE(nodeTwo.state() == STATE);
   REQUIRE(text_adventure::NodeEditor::hasNeighbor(nodeTwo, ID2));
}

TEST_CASE( "Movement", "[Node]")
{
   constexpr auto ID{ "Test" };
   constexpr auto ID2{ "Test2" };
   constexpr auto STATE{ "Test-State" };
   text_adventure::Node node(ID);

   text_adventure::NodeEditor::setState(node, STATE);
   text_adventure::NodeEditor::addNeighbor(node, ID2);


   text_adventure::Action::Move move{ ID2 };

   auto result(node(move));

   REQUIRE(result.target == ID2);
}
