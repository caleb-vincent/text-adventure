//
// Copyright 2022 Caleb Vincent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <catch2/catch.hpp>

#include "node_filter.hpp"

#include <algorithm>
#include <array>

using namespace std::string_literals;

TEST_CASE( "Node Filtering", "[NodeFilter]")
{
   const auto NODE_NAME{ "Test-Name"s };
   const auto STATE_NAME{ "default"s };
   const auto WILDCARD{ "*"s };


   text_adventure::Node node(NODE_NAME);

   SECTION("Exact Match, ID only")
   {
      REQUIRE(text_adventure::NodeFilter::matches(node, NODE_NAME));
   }

   SECTION("Wildcard ID Match")
   {
      REQUIRE(text_adventure::NodeFilter::matches(node, WILDCARD));
   }


   SECTION("Exact ID Match, Exact State Match")
   {
      const auto filterString{ NODE_NAME + ":" + STATE_NAME };
      INFO(filterString);
      REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
   }

   SECTION("Wildcard ID Match, Exact State Match")
   {
      const auto filterString{ WILDCARD + ":" + STATE_NAME };
      INFO(filterString);
      REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
   }


   SECTION("Exact ID Match, Wildcard State Match")
   {
      const auto filterString{ NODE_NAME + ":" + WILDCARD };
      INFO(filterString);
      REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
   }

   SECTION("Wildcard ID Match, Wildcard State Match")
   {
      const auto filterString{ WILDCARD + ":" + WILDCARD };
      INFO(filterString);
      REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
   }
}



TEST_CASE( "Node Filtering, with properties", "[NodeFilter]")
{
   const auto NODE_NAME{ "Test-Name"s };
   const auto STATE_NAME{ "default"s };
   const auto WILDCARD{ "*"s };
   constexpr auto PROPERTIES{ std::array{ "dark", "lIght", "un-done"} };
   constexpr auto INVALID_PROPERTIES{ std::array{ "dARK", "light", "up-dog"} };



   text_adventure::Node node(NODE_NAME.data());

   for(auto const& prop : PROPERTIES)
   {
      node.property(prop, "val");
   }

   SECTION("Valid Properties")
   {
      for(auto count{1u}; count <= PROPERTIES.size(); ++count)
      {
         auto propertyList{ "{"s + PROPERTIES[0]};
         for(auto index{1u}; index < count; ++index)
         {
            propertyList += ","s + PROPERTIES[index];
         }
         propertyList += "}";

         {
            const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
            INFO(filterString);
            REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
         }

         {
            const auto filterString{  NODE_NAME + propertyList };
            INFO(filterString);
            REQUIRE(text_adventure::NodeFilter::matches(node, filterString));
         }
      }
   }

   SECTION("Invalid Properties")
   {
      for(auto count{1u}; count <= INVALID_PROPERTIES.size(); ++count)
      {
         auto propertyList{ "{"s + INVALID_PROPERTIES[0]};
         for(auto index{1u}; index < count; ++index)
         {
            propertyList += ","s + INVALID_PROPERTIES[index];
         }
         propertyList += "}";

         {
            const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
            INFO(filterString);
            REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
         }

         {
            const auto filterString{  NODE_NAME + propertyList };
            INFO(filterString);
            REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
         }
      }
   }


   SECTION("Mixed Properties")
   {
      std::vector<decltype(PROPERTIES)::value_type> properties( PROPERTIES.size() + INVALID_PROPERTIES.size() );
      std::ranges::merge(PROPERTIES, INVALID_PROPERTIES, properties.begin());

      auto propertyList{ "{"s + properties[0] };
      for(auto index{1u}; index < properties.size(); ++index)
      {
         propertyList += ","s + properties[index];
      }
      propertyList += "}";

      {
         const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
         INFO(filterString);
         REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
      }

      {
         const auto filterString{  NODE_NAME + propertyList };
         INFO(filterString);
         REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
      }
   }


   SECTION("Single missing Properties")
   {
      auto propertyList{ "{Old-Not-Me"s };
      for(auto index{0u}; index < PROPERTIES.size(); ++index)
      {
         propertyList += ","s + PROPERTIES[index];
      }
      propertyList += "}";

      {
         const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
         INFO(filterString);
         REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
      }

      propertyList = "{"s + PROPERTIES[0];
      for(auto index{1u}; index < PROPERTIES.size(); ++index)
      {
         propertyList += ","s + PROPERTIES[index];
      }
      propertyList += ",I-Didnt-do_IT}";

      {
         const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
         INFO(filterString);
         REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
      }

      propertyList = "{"s + PROPERTIES[0];
      for(auto index{1u}; index < PROPERTIES.size()-1; ++index)
      {
         propertyList += ","s + PROPERTIES[index];
      }
      propertyList += ",iswear,"s + PROPERTIES.back() + "}";

      {
         const auto filterString{  NODE_NAME + propertyList + ":" + STATE_NAME };
         INFO(filterString);
         REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, filterString));
      }
   }

}

TEST_CASE( "Node Filtering, with properties & values", "[NodeFilter]")
{
   const auto NODE_NAME{ "Test-Name"s };
   const auto STATE_NAME{ "default"s };
   const auto WILDCARD{ "*"s };
   constexpr auto PROPERTIES{ std::array{ "dark", "lIght", "un-done"} };


   text_adventure::Node node(NODE_NAME.data());

   const auto FILTER_ONE{ NODE_NAME + "{broke=yes}:" + STATE_NAME };
   SECTION("Test Filter-- "s + FILTER_ONE)
   {
      node.property("broke", "yes");
      INFO("Set to "s + node.id() + "{broke=" + node.property("broke") + "}:" + node.state());
      REQUIRE(text_adventure::NodeFilter::matches(node, FILTER_ONE));

      node.property("broke", "no");
      INFO("Set to "s + node.id() + "{broke=" + node.property("broke") + "}:" + node.state());
      REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, FILTER_ONE));
   }

   const auto FILTER_TWO{ NODE_NAME + "{broke=yes,on-fire=very}:" + STATE_NAME };
   SECTION("Test Filter-- "s + FILTER_TWO)
   {
      node.property("broke", "yes");
      INFO("Set to "s + node.id() + "{broke=" + node.property("broke") + "}:" + node.state());
      REQUIRE_FALSE(text_adventure::NodeFilter::matches(node, FILTER_TWO));

      node.property("on-fire", "very");
      INFO("Set to "s + node.id() + "{broke=" + node.property("broke") + ",on-fire=very}:" + node.state());
      REQUIRE(text_adventure::NodeFilter::matches(node, FILTER_TWO));
   }
}
